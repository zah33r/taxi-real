﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSLogger;

namespace TaxiReal.DTO
{
    public class SERVICE_DETAIL
    {
        public int Id { get; set; }
        public decimal IDSERVICE { get; set; }
        public decimal ITEMNUMBER { get; set; }
        public string ADDRESS { get; set; }
        public string ZONE { get; set; }
        public string COORDINATES { get; set; }

        public bool save()
        {
            try
            {
                string Query = @"INSERT INTO [SERVICE_DETAIL]([IDSERVICE],[ITEMNUMBER],[ADDRESS],[ZONE],[COORDINATES])
                                VALUES(" + IDSERVICE + "," + ITEMNUMBER + ",'" + ADDRESS + "','" + ZONE + "','" + COORDINATES + "')";

                return DataAccess.ExecuteNonQuery(System.Data.CommandType.Text, Query, null) > 0;
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
                throw;
            }
        }
    }
}
