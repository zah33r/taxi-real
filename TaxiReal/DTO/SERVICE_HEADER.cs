﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSLogger;

namespace TaxiReal.DTO
{
    public class SERVICE_HEADER
    {
        public int Id { get; set; }
        public decimal IDSERVICE { get; set; }
        public decimal ACCOUNTNUMBER { get; set; }
        public string ACCOUNTNAME { get; set; }
        public System.DateTime DATE { get; set; }
        public string START { get; set; }
        public string END { get; set; }
        public string PASSENGER { get; set; }
        public string ROUTE { get; set; }
        public string OBSERVATION { get; set; }
        public string DRIVERNUMBER { get; set; }
        public string VEHICLENUMBER { get; set; }
        public string COSTCENTER { get; set; }
        public string PROJECT { get; set; }
        public string REFERENCE { get; set; }
        public string TYPE_PAYMENT { get; set; }
        public decimal FARE { get; set; }

        public bool save()
        {
            try
            {
                string Query = @"INSERT INTO [SERVICE_HEADER] ([IDSERVICE] ,[ACCOUNTNUMBER] ,[ACCOUNTNAME] ,[DATE] ,[START] ,[END] ,[PASSENGER] ,[ROUTE] ,[OBSERVATION] ,[DRIVERNUMBER] ,[VEHICLENUMBER] ,[COSTCENTER] ,[PROJECT] ,[REFERENCE] ,[TYPE_PAYMENT] ,[FARE])
                                VALUES (" + IDSERVICE + " ," + ACCOUNTNUMBER + " ,'" + ACCOUNTNAME + "','" + DATE + "','" + START + "','" + END + "','" + PASSENGER + "','" + ROUTE + "','" + OBSERVATION + "','" + DRIVERNUMBER + "','" + VEHICLENUMBER + "','" + COSTCENTER + "' ,'" + PROJECT + "','" + REFERENCE + "','" + TYPE_PAYMENT + "'," + FARE + ")";

                return DataAccess.ExecuteNonQuery(System.Data.CommandType.Text, Query, null) > 0;
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
                throw;
            }
        }
    }
}
