﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NSLogger;

namespace TaxiReal
{
    public class LocationManager
    {
        public static string[] GetGeoLocation(string address, string googleApiKey)
        {
            string[] longLatitude = new string[2] { string.Empty, string.Empty };
            try
            {
                if (address.StartsWith("desde:") || address.StartsWith("al:") || address.StartsWith("vía:"))
                {
                    address = address.Substring(address.IndexOf(':') + 1);
                }

                address = address.Trim();
                address = address.Replace(" ", "+");

                string url = @"https://maps.googleapis.com/maps/api/geocode/xml?address=" + address + @"&key=" + googleApiKey;

                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                DataSet dsXML = new DataSet("HTTPResponse");
                dsXML.ReadXml(response.GetResponseStream());

                longLatitude[0] = dsXML.Tables["location"].Rows[0]["lng"].ToString();
                longLatitude[1] = dsXML.Tables["location"].Rows[0]["lat"].ToString();
            }
            catch (Exception Exp)
            {
                Logger.WriteException(Exp);
            }

            return longLatitude;
        }
    }
}
