using System;
using System.Collections.Generic;
using System.Text;

namespace TaxiReal
{
    public abstract class Defaults
    {
        public static string PPhrase = string.Empty;
        public static string SPhrase = string.Empty;
        public static int PIterations = 2;
        public static string IVector = string.Empty;
        public static int KSize = 256;
        public static string HAlgo = string.Empty;
    }  
}