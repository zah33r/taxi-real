﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel;
using NSLogger;
using TaxiReal.DTO;
using TaxiReal.Service;

namespace TaxiReal
{
    public class Route
    {
        #region Variables

        public static string googleApiKey { get; set; }
        private const string SP_GetZoneFromSQL = "TR_CALCULAR_ZONA_POR_PUNTO_GPS";

        #endregion

        #region Methods

        private static void RemoveEmptyRows(DataTable source)
        {
           for( int i = source.Rows.Count-1; i >= 0; i-- )
           {
              DataRow currentRow = source.Rows[i];
              bool isEmpty = true;
              foreach( var colValue in currentRow.ItemArray)
              {
                  if (!string.IsNullOrEmpty(colValue.ToString()))
                  {
                      isEmpty = false;
                      break;
                  }
              }

               if(isEmpty)
                   source.Rows[i].Delete();

           }
           source.AcceptChanges();
        }

        public static DataTable importDataFromExcel(string filePath)
        {
            DataTable res = new DataTable();

            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

                IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                DataSet result = excelReader.AsDataSet();

                excelReader.IsFirstRowAsColumnNames = true;
                result = excelReader.AsDataSet();

                excelReader.Close();
                res = result.Tables[0].Copy();
                RemoveEmptyRows(res);
            }
            catch (Exception exp)
            {
                throw;
            }            

            return res;
        }

        public static DataTable importDataFromExcel(string absoluteFilePath, string sheetName)
        {
            DataTable result = null;

            try
            {
                Logger.WriteLog("Started loading data...", LogLevel.GENERALLOG);
                OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + absoluteFilePath + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'");

                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();

                conn.Open();

                OleDbCommand cmd = new OleDbCommand("select * from [" + sheetName + "$]", conn);
                OleDbDataAdapter adap = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                conn.Close();
                result = ds.Tables[0];
                Logger.WriteLog("Data loading in memory...", LogLevel.GENERALLOG);
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
                result = null;
            }

            return result;
        }

        public static bool exportDataToSQL(DataTable dtExcelData)
        {
            try
            {
                try
                {
                    Logger.WriteLog("Data mapping started...", LogLevel.GENERALLOG);
                    List<SelfService> listOfServices = validateRouteSheet(dtExcelData);
                    Logger.WriteLog("Data mapping ended...", LogLevel.GENERALLOG);

                    Logger.WriteLog("Starting saving data...", LogLevel.GENERALLOG);
                    foreach (SelfService service in listOfServices)
                    {
                        addServiceHeader(service);
                        Logger.WriteLog("Data loaded in service header...", LogLevel.GENERALLOG);
                    }
                    Logger.WriteLog("Information saved successfully...", LogLevel.GENERALLOG);
                }
                catch (DbEntityValidationException dbEntityValidationException)
                {
                    foreach (var eve in dbEntityValidationException.EntityValidationErrors)
                    {
                        Logger.WriteLog("Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:", LogLevel.ERRORLOG);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            Logger.WriteLog("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage, LogLevel.ERRORLOG);
                        }
                    }

                    Logger.WriteException(dbEntityValidationException);
                    return false;
                }
                catch (Exception exp)
                {
                    Logger.WriteException(exp);
                    return false;
                }
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
                return false;
            }

            return true;
        }

        private static bool addServiceHeader(SelfService service)
        {
            foreach (CostCentre cc in service.ListOfCostCentres)
            {
                SERVICE_HEADER objServiceHeader = new SERVICE_HEADER();
                Logger.WriteLog("Looking for data in service header...", LogLevel.GENERALLOG);

                try
                {
                    objServiceHeader.IDSERVICE = Convert.ToDecimal(cc.NoServe.Replace("#", string.Empty));
                    objServiceHeader.ACCOUNTNAME = service.AccountName.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1];
                    objServiceHeader.ACCOUNTNUMBER = Convert.ToDecimal(service.AccountNumber.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0].Trim("#".ToCharArray()));
                    objServiceHeader.COSTCENTER = cc.Name.Contains(':') ? cc.Name.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1] : cc.Name;

                    try
                    {
                        string[] parsedDate = cc.CostCentreDate.Split("//".ToCharArray());
                        objServiceHeader.DATE = new DateTime(Convert.ToInt32(parsedDate[2]), Convert.ToInt32(parsedDate[0]), Convert.ToInt32(parsedDate[1]));
                    }
                    catch (Exception dateExcption)
                    {
                        Logger.WriteException(dateExcption);
                    }
                    finally
                    {
                        objServiceHeader.DATE = DateTime.Parse(cc.CostCentreDate);
                    }

                    objServiceHeader.DRIVERNUMBER = cc.Vehiculo.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                    objServiceHeader.VEHICLENUMBER = cc.Vehiculo.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0].ToString().Split(" ".ToCharArray())[1];
                    objServiceHeader.END = cc.Fin.ToString();
                    objServiceHeader.FARE = cc.Tarifa.Trim() == "-" ? 0 : Convert.ToDecimal(cc.Tarifa);
                    objServiceHeader.OBSERVATION = !cc.Ruta.Contains("Información:") ? string.Empty : cc.Ruta.Substring(cc.Ruta.LastIndexOf("Información:"));
                    objServiceHeader.PASSENGER = cc.Pasajero;
                    objServiceHeader.PROJECT = cc.Proyecto;
                    objServiceHeader.REFERENCE = cc.Reference;
                    
                    if(!string.IsNullOrEmpty( objServiceHeader.OBSERVATION))
                        objServiceHeader.ROUTE = cc.Ruta.Contains(objServiceHeader.OBSERVATION) ? cc.Ruta.Replace(objServiceHeader.OBSERVATION, string.Empty) : cc.Ruta;
                    else
                        objServiceHeader.ROUTE = cc.Ruta;

                    objServiceHeader.START = cc.Inicio;
                    objServiceHeader.TYPE_PAYMENT = cc.Pago;

                    objServiceHeader.save();
                }
                catch (Exception exp)
                {
                    Logger.WriteException(exp);
                    throw;
                }

                try
                {
                    Logger.WriteLog("Splitting route information...", LogLevel.GENERALLOG);
                    List<string> desdeViaAl = parseRoute(objServiceHeader);
                    Logger.WriteLog("Routes extraction completed...", LogLevel.GENERALLOG);

                    for (int index = 0; index < desdeViaAl.Count; index++)
                    {
                        addServiceDetails(objServiceHeader, desdeViaAl, index);
                    }
                }
                catch (Exception exp)
                {
                    Logger.WriteException(exp);
                    throw;
                }
            }

            return true;
        }

        private static List<SelfService> validateRouteSheet(DataTable dtExcelData)
        {
            Hashtable serviceCostHash = new Hashtable();
            Hashtable result_ = new Hashtable();
            List<SelfService> selfServices = new List<SelfService>();

            #region Meta parsing
            try
            {
                dtExcelData = dtExcelData.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                List<DataRow> service_por_cuenta = dtExcelData.Rows.Cast<DataRow>().Where(
                        row => row.ItemArray.Any(
                            field =>
                            {
                                bool result = field.Equals(TaxiReal.Service.RouteConstants.Servicios_por_cuenta);
                                return result;
                            }
                        )
                    ).ToList<DataRow>();

                List<DataRow> centro_de_costo = dtExcelData.Rows.Cast<DataRow>().Where(
                        row => row.ItemArray.Any(
                            field =>
                            {
                                bool result = field.ToString().TrimStart().StartsWith(TaxiReal.Service.RouteConstants.Centro_de_costos);
                                return result;
                            }
                        )
                    ).ToList<DataRow>();

                for (int i = service_por_cuenta.Count - 1; i >= 0; i--)
                {
                    for (int j = centro_de_costo.Count - 1; j >= 0; j--)
                    {
                        if (serviceCostHash.ContainsKey(service_por_cuenta[i]))
                        {
                            if (dtExcelData.Rows.IndexOf(centro_de_costo[j]) > dtExcelData.Rows.IndexOf(service_por_cuenta[i]))
                            {
                                List<DataRow> value = (List<DataRow>)serviceCostHash[service_por_cuenta[i]];
                                value.Add(centro_de_costo[j]);
                                serviceCostHash[service_por_cuenta[i]] = value;
                                centro_de_costo.Remove(centro_de_costo[j]);
                            }
                            else
                                break;
                        }
                        else
                        {
                            if (dtExcelData.Rows.IndexOf(centro_de_costo[j]) > dtExcelData.Rows.IndexOf(service_por_cuenta[i]))
                            {
                                List<DataRow> value = new List<DataRow>();
                                value.Add(centro_de_costo[j]);
                                serviceCostHash.Add(service_por_cuenta[i], value);
                                centro_de_costo.Remove(centro_de_costo[j]);
                            }
                            else
                                break;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }
            #endregion

            #region Data parsing
            try
            {
                foreach (var key in serviceCostHash.Keys)
                {
                    List<DataRow> value = (List<DataRow>)serviceCostHash[key];

                    SelfService objSelfService = new SelfService();
                    objSelfService.AccountName = ((DataRow)key)[16].ToString();
                    objSelfService.AccountNumber = ((DataRow)key)[16].ToString();

                    int indexOf1stRow = dtExcelData.Rows.IndexOf((DataRow)key);
                    objSelfService.DateCreated = dtExcelData.Rows[indexOf1stRow + 1][23].ToString();
                    objSelfService.Period = dtExcelData.Rows[indexOf1stRow + 2][5].ToString();

                    List<CostCentre> selfServiceCostCentre = new List<CostCentre>();

                    #region CostCentres
                    
                    for (int rowCounter = value.Count - 1; rowCounter >= 0; rowCounter--)
                    {
                        DataRow row = value[rowCounter];

                        try
                        {
                            int indexOfCentro_de_costos = dtExcelData.Rows.IndexOf(row);
                            if (indexOf1stRow < 0)
                                continue;

                            DataRow HeaderRow = dtExcelData.Rows[indexOfCentro_de_costos - 2];

                            if (result_.Count == 0)
                            {
                                List<object> headersList = HeaderRow.ItemArray.ToList();
                                result_.Add(RouteConstants.No_serv, headersList.IndexOf(RouteConstants.No_serv));
                                result_.Add(RouteConstants.Inicio, headersList.IndexOf(RouteConstants.Inicio));
                                result_.Add(RouteConstants.Fin, headersList.IndexOf(RouteConstants.Fin));
                                result_.Add(RouteConstants.Pasajero, headersList.IndexOf(RouteConstants.Pasajero));
                                result_.Add(RouteConstants.Ruta, headersList.IndexOf(RouteConstants.Ruta));
                                result_.Add(RouteConstants.Vehículo, headersList.IndexOf(RouteConstants.Vehículo));
                                result_.Add(RouteConstants.Proyecto, headersList.IndexOf(RouteConstants.Proyecto));
                                result_.Add(RouteConstants.Pago, headersList.IndexOf(RouteConstants.Pago));
                                result_.Add(RouteConstants.Tarifa, headersList.IndexOf(RouteConstants.Tarifa));
                            }

                            int nextRowIndex = addCostCentreToService(dtExcelData, result_, selfServiceCostCentre, indexOfCentro_de_costos);

                            while (true && (nextRowIndex < dtExcelData.Rows.Count))
                            {
                                try
                                {                                    
                                    DataRow ValuesRow = dtExcelData.Rows[nextRowIndex];
                                    if (ValuesRow.ItemArray.Any(objString => { return objString.ToString() == RouteConstants.Sumatoria_CC; }))
                                        break;

                                    if (ValuesRow.ItemArray.Any(objString => { return objString.ToString().StartsWith(RouteConstants.Centro_de_costos); }))
                                        break;

                                    if (ValuesRow.ItemArray[2].ToString().StartsWith("#"))  //found Id cell, needs to add this
                                    {
                                        int currentRow = nextRowIndex - 2;
                                        nextRowIndex = addCostCentreToService(dtExcelData, result_, selfServiceCostCentre, currentRow);
                                    }
                                }
                                catch (Exception innerExp)
                                {
                                    Logger.WriteException(innerExp);
                                }

                                nextRowIndex++;
                            }
                        }
                        catch (Exception innerExp)
                        {
                            Logger.WriteException(innerExp);
                        }                        
                    }
                    #endregion

                    objSelfService.ListOfCostCentres = selfServiceCostCentre;
                    selfServices.Add(objSelfService);
                    result_.Clear();
                }
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }
            #endregion

            return selfServices;
        }

        private static int addCostCentreToService(DataTable dtExcelData, Hashtable result_, List<CostCentre> selfServiceCostCentre, int indexOfCentro_de_costos)
        {
            DataRow ValuesRow = dtExcelData.Rows[indexOfCentro_de_costos + 2];

            CostCentre objCostCentre = new CostCentre();
            objCostCentre.Name = dtExcelData.Rows[indexOfCentro_de_costos][2].ToString();
            objCostCentre.NoServe = ValuesRow[(Int32)result_[RouteConstants.No_serv] - 2].ToString();   //due to merged cells there is a displacement in cells
            objCostCentre.Inicio = ValuesRow[(Int32)result_[RouteConstants.Inicio] - 1].ToString();     //due to merged cells there is a displacement in cells
            objCostCentre.Fin = ValuesRow[(Int32)result_[RouteConstants.Fin]].ToString();
            objCostCentre.Pasajero = ValuesRow[(Int32)result_[RouteConstants.Pasajero]].ToString();
            objCostCentre.Ruta = ValuesRow[(Int32)result_[RouteConstants.Ruta]].ToString();
            objCostCentre.Vehiculo = ValuesRow[(Int32)result_[RouteConstants.Vehículo]].ToString();
            objCostCentre.Proyecto = ValuesRow[(Int32)result_[RouteConstants.Proyecto]].ToString();

            DataRow refRow = dtExcelData.Rows[indexOfCentro_de_costos + 3];
            objCostCentre.Reference = refRow[(Int32)result_[RouteConstants.Proyecto]].ToString();

            objCostCentre.Pago = ValuesRow[(Int32)result_[RouteConstants.Pago]].ToString();
            objCostCentre.Tarifa = ValuesRow[(Int32)result_[RouteConstants.Tarifa]].ToString();

            DataRow costCentreDateRow = dtExcelData.Rows[indexOfCentro_de_costos + 1];
            objCostCentre.CostCentreDate = costCentreDateRow[2].ToString();

            if (objCostCentre.CostCentreDate.Trim() == string.Empty || objCostCentre.CostCentreDate.Trim().StartsWith("#"))
            { 
                if(selfServiceCostCentre.Count > 0 )
                {
                    objCostCentre.CostCentreDate = selfServiceCostCentre[selfServiceCostCentre.Count - 1].CostCentreDate;
                }
            }

            if (objCostCentre.Name.StartsWith("#") || objCostCentre.Name.Trim() == string.Empty || objCostCentre.Name.Contains("/") )
            {
                if (selfServiceCostCentre.Count > 0)
                {
                    objCostCentre.Name = selfServiceCostCentre[selfServiceCostCentre.Count - 1].Name;
                }
            }

            selfServiceCostCentre.Add(objCostCentre);
            return indexOfCentro_de_costos + 3;
        }

        private static List<string> parseRoute(SERVICE_HEADER objServiceHeader)
        {
            List<string> desdeViaAl = null;
            try
            {
                string Route = objServiceHeader.ROUTE;
                Route = Route.Replace("desde:", Environment.NewLine + "desde:");
                Route = Route.Replace("vía:", Environment.NewLine + "vía:");
                Route = Route.Replace("al:", Environment.NewLine + "al:");

                desdeViaAl = Route.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(s =>
                {
                    return s.Trim();
                }).ToList<string>();
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }

            desdeViaAl.Remove(string.Empty);
            return desdeViaAl;
        }

        [Obsolete("This method was used when static cell values were being picked up from sheet. It has been replaced with dynamic logic now", true)]
        private static SERVICE_HEADER addServiceHeader(DataTable dtExcelData)
        {
            Logger.WriteLog("Looking for data in service header...", LogLevel.GENERALLOG);
            SERVICE_HEADER objServiceHeader = new SERVICE_HEADER();

            try
            {
                objServiceHeader.IDSERVICE = Convert.ToDecimal(dtExcelData.Rows[15][2].ToString().Replace("#", string.Empty));                                                                      //18070804;
                objServiceHeader.ACCOUNTNAME = dtExcelData.Rows[44][16].ToString().Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1];                                              //"APROLAB";
                objServiceHeader.ACCOUNTNUMBER = Convert.ToDecimal(dtExcelData.Rows[44][16].ToString().Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0].Trim("#".ToCharArray())); //1880;
                objServiceHeader.COSTCENTER = dtExcelData.Rows[13][2].ToString().Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1];                                                //"aaaaa";

                string[] parsedDate = dtExcelData.Rows[14][2].ToString().Split("//".ToCharArray());                                                                                                 //DateTime.Now;
                objServiceHeader.DATE = new DateTime(Convert.ToInt32(parsedDate[0]), Convert.ToInt32(parsedDate[1]), Convert.ToInt32(parsedDate[2]));

                objServiceHeader.DRIVERNUMBER = dtExcelData.Rows[15][17].ToString().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];                                             //"1013";
                objServiceHeader.VEHICLENUMBER = dtExcelData.Rows[15][17].ToString().Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0].ToString().Split(" ".ToCharArray())[1];    //"750";
                objServiceHeader.END = dtExcelData.Rows[15][9].ToString();                                                                                                                          //"11:59";
                objServiceHeader.FARE = Convert.ToDecimal(dtExcelData.Rows[15][28].ToString());                                                                                                     //25.00M;
                objServiceHeader.OBSERVATION = dtExcelData.Rows[15][13].ToString().Substring(dtExcelData.Rows[15][13].ToString().LastIndexOf("Información:"));                                      //"Recoger en el pto. Referencia.";
                objServiceHeader.PASSENGER = dtExcelData.Rows[15][12].ToString();                                                                                                                   //"Jean Silva";
                objServiceHeader.PROJECT = dtExcelData.Rows[15][20].ToString();                                                                                                                     //"CCCCC";
                objServiceHeader.REFERENCE = dtExcelData.Rows[16][20].ToString();                                                                                                                   //"BBBBB";
                objServiceHeader.ROUTE = dtExcelData.Rows[15][13].ToString().Replace(objServiceHeader.OBSERVATION, string.Empty);                                                                   //"desde: Aurelio Miro Quesada, San Isidro, \nPerúal: Jr. Trujillo, Magdalena del Mar, Perú";
                objServiceHeader.START = dtExcelData.Rows[15][7].ToString();                                                                                                                        //"11:58";
                objServiceHeader.TYPE_PAYMENT = dtExcelData.Rows[15][25].ToString();                                                                                                                //"F";

                //Save service header here
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }

            return objServiceHeader;
        }

        private static void addServiceDetails(SERVICE_HEADER objServiceHeader, List<string> desdeViaAl, int index)
        {
            try
            {
                SERVICE_DETAIL objServiceDetails = new SERVICE_DETAIL();
                objServiceDetails.IDSERVICE = objServiceHeader.IDSERVICE;
                objServiceDetails.ITEMNUMBER = index + 1;
                objServiceDetails.ADDRESS = desdeViaAl[index].Trim();

                string zone = extractZoneFromAddress(objServiceDetails.ADDRESS);

                Logger.WriteLog("Getting geolocation from address...", LogLevel.GENERALLOG);
                string[] lngLat = LocationManager.GetGeoLocation(objServiceDetails.ADDRESS, googleApiKey);
                objServiceDetails.COORDINATES = lngLat[0] + "," + lngLat[1];
                Logger.WriteLog("Geolocation fetching completed...", LogLevel.GENERALLOG);

                Logger.WriteLog("Getting zone from geolocation...", LogLevel.GENERALLOG);
                if (zone.Trim() == string.Empty)
                    objServiceDetails.ZONE = getZoneByLongitudeLatitude(lngLat[1], lngLat[0]);
                else
                    objServiceDetails.ZONE = zone;
                Logger.WriteLog("Zone identification completed...", LogLevel.GENERALLOG);

                objServiceDetails.save();
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
                throw exp;
            }
        }

        private static string extractZoneFromAddress(string address)
        {
            string result = string.Empty;

            try
            {
                if (address.Trim().EndsWith(")"))
                {
                    int lastIndexOfOpenBracket = address.LastIndexOf("(");

                    string value = address.Substring(lastIndexOfOpenBracket + 1);
                    value = value.Replace(")", string.Empty);
                    int iValue = -1;
                    if (Int32.TryParse(value, out iValue))
                        result = iValue.ToString();
                }
                else if (address.Trim().ToLower().StartsWith("al:"))
                {
                    address = address.Replace("al:", string.Empty);
                    string[] ZoneID = address.Split(",".ToCharArray());
                    int zoneValue = -1;
                    if (Int32.TryParse(ZoneID[0].Trim(), out zoneValue))
                        result = zoneValue.ToString();
                }
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }

            return result;
        }

        public static string getZoneByLongitudeLatitude(string latidude, string longitude)
        {
            string result = string.Empty;

            try
            {
                DataTable dtResult = ((DataSet)DataAccess.ExecuteDataSet(CommandType.StoredProcedure, SP_GetZoneFromSQL, new SqlParameter[2] { new SqlParameter("@latitud", latidude), 
                                                                                                                new SqlParameter("@longitud", longitude) })).Tables[0];
                result = dtResult.Rows[0][1].ToString();
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
                throw exp;
            }

            return result;
        }

        #endregion
    }
}