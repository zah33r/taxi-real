﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiReal.Service
{
    public class CostCentre
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string noServe;

        public string NoServe
        {
            get { return noServe; }
            set { noServe = value; }
        }

        private string inicio;

        public string Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }

        private string fin;

        public string Fin
        {
            get { return fin; }
            set { fin = value; }
        }

        private string pasajero;

        public string Pasajero
        {
            get { return pasajero; }
            set { pasajero = value; }
        }

        private string ruta;

        public string Ruta
        {
            get { return ruta; }
            set { ruta = value; }
        }

        private string vehiculo;

        public string Vehiculo
        {
            get { return vehiculo; }
            set { vehiculo = value; }
        }

        private string proyecto;

        public string Proyecto
        {
            get { return proyecto; }
            set { proyecto = value; }
        }

        private string pago;

        public string Pago
        {
            get { return pago; }
            set { pago = value; }
        }

        private string tarifa;

        public string Tarifa
        {
            get { return tarifa; }
            set { tarifa = value; }
        }

        private string costCentreDate;

        public string CostCentreDate
        {
            get { return costCentreDate; }
            set { costCentreDate = value; }
        }

        private string reference;

        public string Reference
        {
            get { return reference; }
            set { reference = value; }
        }


    }
}
