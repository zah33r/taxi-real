﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiReal.Service
{
    public class RouteConstants
    {
        public const string Servicios_por_cuenta = "Servicios por cuenta";
        public const string periodo = "periodo";
        public const string No_serv = "No. serv.";
        public const string Inicio = "Inicio";
        public const string Fin = "Fin";
        public const string Pasajero = "Pasajero";
        public const string Ruta = "Ruta";
        public const string Vehículo = "Vehículo";
        public const string Proyecto = "Proyecto";
        public const string Pago = "Pago";
        public const string Tarifa = "Tarifa";
        public const string Centro_de_costos = "Centro de costos";
        public const string Sumatoria_CC = "Sumatoria CC";
    }
}
