﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiReal.Service
{
    public class SelfService
    {
        private string dateCreated;

        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        private string accountNumber;

        public string AccountNumber
        {
            get { return accountNumber; }
            set { accountNumber = value; }
        }

        private string accountName;

        public string AccountName
        {
            get { return accountName; }
            set { accountName = value; }
        }

        private string period;

        public string Period
        {
            get { return period; }
            set { period = value; }
        }

        private List<CostCentre> listOfCostCentres;

        public List<CostCentre> ListOfCostCentres
        {
            get { return listOfCostCentres; }
            set { listOfCostCentres = value; }
        }

    }
}
