using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using NSLogger;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Data;
using System.Globalization;
using System.Threading;

namespace TaxiReal
{
   public abstract class Utility
    {
       public static string GetMAC()
       {
           string MACID = "";
           try
           {
               System.Management.ManagementClass mc = new System.Management.ManagementClass("Win32_NetworkAdapterConfiguration");
               System.Management.ManagementObjectCollection moc = mc.GetInstances();

               foreach (System.Management.ManagementObject mo in moc)
               {
                   if ((bool)mo["IPEnabled"] == true)
                       MACID = mo["MacAddress"].ToString();

                   mo.Dispose();
               }
               return MACID;
           }
           catch
           {
           }
           return MACID;
       }

       public static string CheckHostName(string IPAddress)
       {
           string strHostDomain = "";
           string strHostName = "";
           try
           {
               System.Net.IPHostEntry ip = System.Net.Dns.GetHostEntry(IPAddress);
               strHostDomain = ip.HostName.ToString();
               for (int i = 0; i < strHostDomain.Length; i++)
               {
                   if (strHostDomain[i] != '.')
                       strHostName += strHostDomain[i].ToString();
                   else
                       break;
               }
               return strHostName;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
               return IPAddress;
           }
       }

       public static string GetLocalIP()
       {
           string IP = string.Empty;

           try
           {
               IPAddress[] IPAdrees = Dns.GetHostByName(Dns.GetHostName()).AddressList;
               for (int i = 0; i < IPAdrees.Length; i++)
                   IP = IPAdrees[i].ToString();
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
           }
           return IP;
       }

       public static string CheckIPAddress(string HostName)
       {
           string strHostDomain = "";

           try
           {
               System.Net.IPHostEntry ip = System.Net.Dns.GetHostEntry(HostName);
               strHostDomain = ip.AddressList[0].ToString();
               return strHostDomain;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
               return HostName;
           }
       }

       public static string GetConnectionString()
       {
           try
           {
               string Path_ = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Configuration\\DatabaseSettings.xml";

               if (!File.Exists(Path_))
                   return string.Empty;

               XmlDocument xmlDoc = new XmlDocument();
               xmlDoc.Load(Path_);
               XmlNodeList SQLConnNode = xmlDoc.GetElementsByTagName("SQLConn");
               return SQLConnNode[0].InnerText;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
           }
           return string.Empty;
       }

       public static object ByteArrayToObject(byte[] ByteArr)
       {
           object obj = null;
           try
           {
               if (ByteArr.Length > 0)
               {
                   MemoryStream ms = new MemoryStream(ByteArr);
                   BinaryFormatter formatter = new BinaryFormatter();

                   formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                   formatter.TypeFormat = FormatterTypeStyle.TypesAlways;
                   obj = (object)formatter.Deserialize(ms);

                   formatter = null;
                   ms.Close();
                   ms = null;
               }
           }
           catch (Exception E)
           {
               string strEx = E.Message;
           }
           return obj;
       }

       public static byte[] ObjectToByteArray(object objConfig)
       {
           try
           {
               if (objConfig != null)
               {
                   MemoryStream ms = new MemoryStream();
                   BinaryFormatter formatter = new BinaryFormatter();
                   formatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
                   formatter.TypeFormat = FormatterTypeStyle.TypesAlways;
                   formatter.Serialize(ms, objConfig);
                   Byte[] byteToSend = ms.ToArray();
                   formatter = null;
                   ms.Close();
                   ms = null;
                   return byteToSend;
               }
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
           }
           return null;
       }

       public static bool isEmail(string inputEmail)
       {
           try
           {
               if (inputEmail != null && inputEmail.Trim().ToString() != "")
               {
                   string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                       @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                       @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                   Regex re = new Regex(strRegex);
                   if (re.IsMatch(inputEmail))
                       return (true);
                   else
                       return (false);
               }

           }
           catch (Exception E)
           {
               Logger.WriteException(E);
           }
           return false;
       }

       public static string ConvertToValidPhoneToDail(string Str)
       {
           try
           {
               string RStr = "";
               if (Str != null)
               {
                   for (int j = 0; j < Str.Length; j++)
                   {
                       if (Str.ToCharArray()[j] == '1' || Str.ToCharArray()[j] == '2' || Str.ToCharArray()[j] == '3' || Str.ToCharArray()[j] == '4' || Str.ToCharArray()[j] == '5' || Str.ToCharArray()[j] == '6' || Str.ToCharArray()[j] == '7' || Str.ToCharArray()[j] == '8' || Str.ToCharArray()[j] == '9' || Str.ToCharArray()[j] == '0')
                           RStr = RStr + Str.ToCharArray()[j];
                   }
               }
               return RStr;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
               return string.Empty;
           }

       }

       public static bool SetDefaults(string Information)
       {
           try
           {
               byte[] bArr = Convert.FromBase64String(Information);
               string[] KeyDetails = Encoding.ASCII.GetString(bArr).Split("^".ToCharArray());

               Defaults.PPhrase = KeyDetails[0];
               Defaults.SPhrase = KeyDetails[1];
               Defaults.PIterations = Convert.ToInt32(KeyDetails[2]);
               Defaults.IVector = KeyDetails[3];
               Defaults.KSize = Convert.ToInt32(KeyDetails[4]);
               Defaults.HAlgo = KeyDetails[5];

               return true;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
               return false;
           }
       }

       public static string Encrypt(string InputText)
       {
           try
           {
               byte[] initVectorBytes = Encoding.ASCII.GetBytes(Defaults.IVector);
               byte[] saltValueBytes = Encoding.ASCII.GetBytes(Defaults.SPhrase);
               byte[] plainTextBytes = Encoding.UTF8.GetBytes(InputText);
               PasswordDeriveBytes password = new PasswordDeriveBytes(Defaults.PPhrase, saltValueBytes, Defaults.HAlgo, Defaults.PIterations);
               byte[] keyBytes = password.GetBytes(Defaults.KSize / 8);
               RijndaelManaged symmetricKey = new RijndaelManaged();
               symmetricKey.Mode = CipherMode.CBC;
               ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
               MemoryStream memoryStream = new MemoryStream();
               CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
               cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
               cryptoStream.FlushFinalBlock();
               byte[] cipherTextBytes = memoryStream.ToArray();
               memoryStream.Close();
               cryptoStream.Close();
               string cipherText = Convert.ToBase64String(cipherTextBytes);
               return cipherText;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
           }
           return string.Empty;
       }

       public static string Decrypt(string InputText)
       {
           try
           {

               byte[] initVectorBytes = Encoding.ASCII.GetBytes(Defaults.IVector);
               byte[] saltValueBytes = Encoding.ASCII.GetBytes(Defaults.SPhrase);
               byte[] cipherTextBytes = Convert.FromBase64String(InputText);
               PasswordDeriveBytes password = new PasswordDeriveBytes(Defaults.PPhrase, saltValueBytes, Defaults.HAlgo, Defaults.PIterations);
               byte[] keyBytes = password.GetBytes(Defaults.KSize / 8);
               RijndaelManaged symmetricKey = new RijndaelManaged();
               symmetricKey.Mode = CipherMode.CBC;
               ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
               MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
               CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
               byte[] plainTextBytes = new byte[cipherTextBytes.Length];
               int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
               memoryStream.Close();
               cryptoStream.Close();
               string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
               return plainText;
           }
           catch (Exception E)
           {
               Logger.WriteException(E);
           }
           return string.Empty;
       }
    }
}
