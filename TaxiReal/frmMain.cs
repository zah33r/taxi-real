﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSLogger;

namespace TaxiReal
{
    public partial class frmMain : Form
    {
        #region Variables

        public static string StartUpPath = string.Empty;
        private string absoluteFilePath;

        #endregion

        #region ctor

        public frmMain()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void frmMain_Load(object sender, EventArgs e)
        {
            setTraces();
            DataAccess.CON_STR = System.Configuration.ConfigurationManager.ConnectionStrings["TaxiDB"].ConnectionString;
        }

        private void testRun()
        {
            //int rows = ds.Tables[0].Rows.Count;
            //int columns = ds.Tables[0].Columns.Count;

            //for (int rowIndex = 0; rowIndex < rows; rowIndex++)
            //{
            //    for (int columnIndex = 0; columnIndex < columns; columnIndex++)
            //    {
            //        string cellValue = ds.Tables[0].Rows[rowIndex][columnIndex].ToString();

            //        if (!string.IsNullOrEmpty(cellValue.Trim()))
            //        {
            //            textBox1.Text += Environment.NewLine + "Position: " + rowIndex + ", " + columnIndex + " Value: " + cellValue;
            //        }
            //    }
            //}

            //string result = ExcelManager.GetZoneByLongitudeLatitude("121", "121");

            //string[] longitudeLatitude = GetGeoLocation(@"1600 Amphitheatre Parkway, Mountain View, CA", @"AIzaSyBHr7kcAYx6RtHU8lq1kTHs71cGcl29-xo");
            //return;
        }

        private void beginProcess()
        {
            try
            {
                DataTable dtSheet = Route.importDataFromExcel(@"" + txtFilePath.Text.Trim(), @"" + txtSheetName.Text.Trim());
                if (Route.exportDataToSQL(dtSheet))
                {
                    MessageBox.Show("Process completed successfully", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Logger.WriteLog("Process completed successfully", LogLevel.GENERALLOG);
                }
                else
                {
                    MessageBox.Show("Process failed to complete, please try again.\nOr check logs for more details.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Logger.WriteLog("Process failed to complete, please try again.\nOr check logs for more details.", LogLevel.GENERALLOG);
                }
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }
        }        

        public static void setTraces()
        {
            Logger.EnableCOMMSLOG = Logger.EnableDBLOG = Logger.EnableERRORLOG = Logger.EnableGENERALLOG = Logger.EnableSECURELOG = Logger.EnableTELEPHONYLOG = true;

            Logger.FilePath = StartUpPath;
            Logger.LogFileName = "Taxi Real Logs";
            Logger.LogFileSize = 1024;
            Logger.NoOfLogFiles = 10;
            Logger.ActivateOptions();

            Logger.WriteLog("Logs Activated.", LogLevel.GENERALLOG);
        }

        private void btnImportData_Click(object sender, EventArgs e)
        {
            try
            {
                btnImportData.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                this.Text += "Processing please wait...";
                beginProcess();
            }
            catch (Exception exp)
            {
                Logger.WriteException(exp);
            }
            finally
            {
                this.Text = this.Text.Replace("Processing please wait...", string.Empty);
                btnImportData.Enabled = true;
                this.Cursor = Cursors.Arrow;
            }
        }

        #endregion
    }
}
